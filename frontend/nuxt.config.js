export default {
    mode: 'universal',
    components: true,
    /*
     ** Headers of the page
     */
    head: {
        title: 'BS Myco Biotech',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
        script: [
            { id: 'ze-snippet', src: 'https://static.zdassets.com/ekr/snippet.js?key=69476f74-d546-4c43-a39f-46209e0a7944' }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Global CSS
     */
    css: [{
            src: '@fortawesome/fontawesome-free/css/all.css',
            lang: 'css'
        },
        '~/assets/css/style.css',
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        '@nuxtjs/pwa',
        'bootstrap-vue/nuxt',
    ],
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}
    }
}